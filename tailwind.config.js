/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        'Space': ['Space Grotesk', 'sans-serif'],
        'OpenSans': ['Open Sans', 'sans-serif'],
        'Inter': ['Inter', 'sans-serif'],
      },
      colors: {
        'blue': {
          '1' : '#2F80ED',
          '2' : '#2D9CDB'
        },
        'gray': {
          '1' : '#333333',
          '2' : '#4F4F4F',
          '3' : '#828282',
          '4' : '#BDBDBD',
          '5' : '#E0E0E0',
          '6' : '#F2F2F2'
        },
        'contact': {
          '1' : '#010101'
        }
      }
    },
  },
  plugins: [],
}
