import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Home from "./home/Home";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import EBook from "./e-book/EBook";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="e-book" element={<EBook />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
