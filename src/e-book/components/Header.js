import React from "react";
import LeftArrow from "../../assets/icons/left-arrow";

export default function Header() {
  return (
    <button className="w-36 h-6">
      <a href="/" className="hidden lg:flex items-center gap-2">
        <LeftArrow />
        <h1 className="font-Space font-bold text-base text-blue-500 uppercase">
          Back To Home
        </h1>
      </a>
    </button>
  );
}
