import React from "react";

export default function Title() {
  return (
    <div className="gap-5 p-3 w-[290px] md:w-[588px] lg:w-[590px]">
      <h1 className="font-Space text-2xl font-semibold text-black">
        Investing in Digital Talent: Strategies for Developing a Skilled
        Workforce
      </h1>
      <h1 className="font-OpenSans font-normal text-sm text-gray-3 gap-1 py-5">
        by <span className="font-semibold">John Doe</span> (Author)
      </h1>
      <p className="font-OpenSans font-normal text-sm text-gray-2">
        Investing in Digital Talent: Strategies for Developing a Skilled
        Workforce is a comprehensive guide that explores the crucial role of
        human capital in the digital era. Authored by industry expert John
        Smith, this insightful e-book presents innovative strategies and best
        practices for organizations to cultivate and retain a highly skilled
        workforce in the ever-evolving digital landscape. Discover practical
        approaches to attract top digital talent, foster a culture of continuous
        learning, and leverage emerging technologies to drive organizational
        success. Gain valuable insights and actionable recommendations to
        navigate the challenges and opportunities in building a future-ready
        workforce. Whether you are an HR professional, business leader, or
        digital enthusiast, this e-book provides the essential knowledge to
        unlock the potential of your workforce and thrive in the digital age.
        <br />
        <br />
        This book will talking about: <br />
        <ul className="list-decimal list-outside">
          <li>Understanding the Digital Talent Landscape</li>
          <li>Identifying Key Digital Skills and Competencies</li>
          <li>Attracting and Recruiting Top Digital Talent</li>
          <li>
            Nurturing a Culture of Continuous Learning and Skill Development
          </li>
          <li>Building Agile Talent Development Programs</li>
          <li>Retaining and Engaging Digital Talent</li>
          <li>Harnessing Technology for Talent Acquisition and Development</li>
          <li>Addressing Skill Gaps and Upskilling Existing Workforce</li>
          <li>
            Collaborating with Educational Institutions and Industry Partners
          </li>
          <li>Future-proofing Talent Strategies for Digital Transformation</li>
        </ul>
      </p>
    </div>
  );
}
