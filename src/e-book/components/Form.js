import React from "react";

export default function Form() {
  return (
    <form className="w-[290px] md:w-[604px] lg:w-[284px] p-3 gap-5 space-y-5 border-gray-5 border-[1px]">
      <h1 className="font-Space font-bold text-xl">Download E-Book</h1>
      <div className="w-full flex flex-col gap-1">
        <label className="font-Space font-bold text-sm md:text-base">
          Full Name
        </label>
        <input
          type="text"
          placeholder="Insert Full Name"
          className="border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-black focus:outline-none"
        />
      </div>
      <div className="w-full flex flex-col gap-1">
        <label className="font-Space font-bold text-sm md:text-base">
          Email
        </label>
        <input
          type="email"
          placeholder="Insert Email Address"
          className="border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-black focus:outline-none"
        />
      </div>
      <div className="w-full flex flex-col gap-1">
        <label className="font-Space font-bold text-sm md:text-base">
          Company Name
        </label>
        <input
          type="email"
          placeholder="Insert Company Name"
          className="border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-black focus:outline-none"
        />
      </div>
      <div className="w-full flex flex-col gap-1">
        <label className="font-Space font-bold text-sm md:text-base">
          Industry
        </label>
        <select className="border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-black focus:outline-none">
          <option>Sagara</option>
        </select>
      </div>
      <div className="w-full flex flex-col gap-1">
        <label className="font-Space font-bold text-sm md:text-base">
          Job Title
        </label>
        <select className="border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-black focus:outline-none">
          <option>Frontend</option>
        </select>
      </div>
      <button className="w-full py-3 bg-blue-2 font-Space font-bold text-white text-sm md:text-base">
        Get E-Book
      </button>
    </form>
  );
}
