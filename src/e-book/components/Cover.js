import React from "react";
import { ImgCover } from "../../data";

export default function Cover() {
  return (
    <div className="border-gray-5 border-[1px] p-2 gap-2">
      <img
        src={ImgCover[0].image}
        className="w-[274px] h-[437px] md:w-[309px] md:h-[493px] lg:w-[269px] lg:h-[430.4px] "
        alt="cover"
      />
    </div>
  );
}
