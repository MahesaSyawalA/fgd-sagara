import React from "react";
import NavbarComponent from "../components/navbarComponent";
import Header from "./components/Header";
import Cover from "./components/Cover";
import Title from "./components/Title";
import Form from "./components/Form";
import FooterComponent from "../components/footerComponent";

export default function EBook() {
  return (
    <div className="w-full">
      <NavbarComponent shadow="visible" types="ebook" />
      <div className="px-10 py-10 md:py-14 lg:py-20 gap-10">
        <Header />
        <div className="flex flex-col lg:flex-row items-center gap-5">
          <Cover />
          <Title />
          <Form />
        </div>
      </div>
      <FooterComponent />
    </div>
  );
}
