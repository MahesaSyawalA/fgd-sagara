import cover from './assets/image/cover.png'
import cover2 from './assets/image/cover2.png'
import cover3 from './assets/image/cover3.png'
import cover4 from './assets/image/cover4.png'
import ic_chevron_up from './assets/icons/chevron-up'
import ic_left_arrow from './assets/icons/left-arrow'
import ic_mail from './assets/icons/mail'
import ic_menu from './assets/icons/menu'
import ic_phone from './assets/icons/phone'
import ic_search from './assets/icons/search'

export const ImgCover = [
  {
    image: cover,
    title: "Sagara Perspective",
    author: "Jhon Doe",
    alt: "Cover PDF"
  },
  {
    image: cover2,
    title: "Sagara Perspective",
    author: "Jhon Doe",
    alt: "Cover PDF"
  },
  {
    image: cover3,
    title: "Sagara Perspective",
    author: "Jhon Doe",
    alt: "Cover PDF"
  },
  {
    image: cover4,
    title: "Sagara Perspective",
    author: "Jhon Doe",
    alt: "Cover PDF"
  },
]