import React, { useEffect, useRef, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import logo from "../assets/image/logo.png";
import Menu from "../assets/icons/menu";
import Close from "../assets/icons/close";

export default function NavbarComponent({ types, shadow }) {
  const [isNav, setIsNav] = useState(false);
  const location = useLocation();
  const pathname = location.pathname;
  const navigate = useNavigate();

  const navbarRef = useRef(null);
  const [activeSection, setActiveSection] = useState("");

  useEffect(() => {
    const handleScroll = () => {
      const navbarHeight = navbarRef.current.offsetHeight;
      const sections = document.querySelectorAll("section");

      sections.forEach((section) => {
        const sectionTop = section.offsetTop - navbarHeight;
        const sectionBottom = sectionTop + section.offsetHeight;

        if (window.scrollY >= sectionTop && window.scrollY < sectionBottom) {
          setActiveSection(section.id);
        }
      });
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const variants = {
    types: {
      home: `${
        activeSection === "home"
          ? "bg-gray-6"
          : "bg-white" && activeSection === "catalog"
          ? "bg-white"
          : "bg-white" && activeSection === "about"
          ? "bg-gray-6"
          : "bg-white" && activeSection === "contact"
          ? "bg-contact-1"
          : "bg-white" && activeSection === ""
          ? "bg-gray-6"
          : "bg-white"
      }`,
      ebook: "bg-white",
    },
    shadow: {
      none: "shadow-none",
      visible: "shadow-lg",
    },
  };

  const handleNavMenu = () => {
    setIsNav(!isNav);
  };

  const handleClick = (id) => {
    const element = document.getElementById(id);
    element.scrollIntoView({ behavior: "smooth" });
    setIsNav(!isNav);
  };

  const list = [
    { label: "Home", id: "home" },
    { label: "E-Book", id: "catalog" },
    { label: "About Us", id: "about" },
    { label: "Contact Us", id: "contact", class: "flex lg:hidden" },
  ];

  return (
    <div
      onClick={handleNavMenu}
      className={`${
        isNav &&
        "fixed h-screen lg:sticky lg:h-auto inset-0 lg:inset-auto top-0 lg:top-auto left-0 lg:left-auto bg-[#333333]/25"
      }`}
    >
      <div
        ref={navbarRef}
        className={`fixed w-full flex flex-col lg:flex-row lg:justify-between lg:items-center px-5 md:px-10 lg:px-20 ${variants.shadow[shadow]} ${variants.types[types]}`}
      >
        <div className="flex flex-col lg:flex-row lg:items-center lg:gap-5">
          <div className="lg:mr-10 flex items-center p-3 lg:p-0">
            <div className="w-full flex justify-center rot">
              <img src={logo} width={87} height={22} alt="logo" />
            </div>
            <div onClick={handleNavMenu}>
              {isNav ? (
                <Close
                  children={`${
                    activeSection === "contact"
                      ? "fill-white"
                      : "fill-[#1C1B1F]"
                  }`}
                />
              ) : (
                <Menu
                  children={`${
                    activeSection === "contact"
                      ? "fill-white"
                      : "fill-[#1C1B1F]"
                  }`}
                />
              )}
            </div>
          </div>
          {list.map((li, i) => (
            <div
              onClick={() =>
                pathname !== "/"
                  ? li.id === "home" && navigate("/")
                  : handleClick(li.id)
              }
            >
              <button
                key={i}
                className={`flex-col lg:items-center p-3 lg:p-4 w-full ${
                  isNav ? "flex" : "hidden lg:flex"
                } ${
                  activeSection === li.id
                    ? "border-b-4 transition-all border-blue-1"
                    : "border-none" &&
                      activeSection === "" &&
                      setActiveSection(list[0].id)
                } ${li.class}`}
              >
                <h1
                  className={`${
                    li.class
                  } font-Space font-normal text-base uppercase
                  ${
                    activeSection === "contact" ? "text-white" : "text-gray-2"
                  }`}
                >
                  {li.label}
                </h1>
              </button>
            </div>
          ))}
        </div>
        <div
          className={`flex-row items-center gap-2 font-Space font-bold text-sm mt-4 lg:mt-0 hidden lg:flex`}
        >
          <button className="bg-blue-1 text-white p-5 uppercase">
            Contact Us
          </button>
        </div>
      </div>
    </div>
  );
}

NavbarComponent.defaultProps = {
  shadow: "none",
};
