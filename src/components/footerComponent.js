import React from "react";
import logo from '../assets/image/logo.png'

export default function FooterComponent() {
  return (
    <div className="bg-[#15171A] text-white flex flex-col py-5 gap-8">
      <div className="flex justify-between items-center px-8 gap-5 lg:gap-0">
        <div className="flex flex-col lg:flex-row gap-4 lg:gap-5 font-OpenSans font-semibold text-base md:text-lg lg:text-xl">
          <a href={"#about"}>About Us</a>
          <a href={"#contact"}>Contact Us</a>
          <a href={"#tos"}>Terms of Services</a>
          <a href={"#privacy"}>Privacy Policy</a>
        </div>
        <div>
          <img src={logo} width={152.25} height={38.5} alt="logo" />
        </div>
      </div>
      <div className="flex justify-start lg:justify-center font-Inter font-medium text-base text-[#858585] px-5 lg:px-0">
        &copy; 2023 Sagara Perspective
      </div>
    </div>
  );
}
