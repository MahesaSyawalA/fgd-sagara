import React from 'react'
import NavbarComponent from '../components/navbarComponent'
import FooterComponent from '../components/footerComponent'
import Hero from './components/Hero'
import Catalog from './components/Catalog'
import Contact from './components/Contact'
import About from './components/About'

export default function Home() {
  return (
    <div>
      <div className='bg-gray-6 sticky inset-0 z-10'>
        <NavbarComponent shadow="none" types="home" />
      </div>
      <div className='pt-10 lg:pt-0'>
        <Hero />
        <Catalog />
        <About />
        <Contact />
        <FooterComponent />
      </div>
    </div>
  )
}
