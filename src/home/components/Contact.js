import React from "react";
import Phone from "../../assets/icons/phone";
import Mail from "../../assets/icons/mail";

export default function Contact() {
  return (
    <section id="contact">
      <div className="bg-contact-1">
        <div className="w-full h-full flex flex-col lg:flex-row p-5 justify-around gap-3 container mx-auto px-5">
          <div className="w-full lg:w-[794px] text-white">
            <div className="h-[58px] w-[155px] md:w-[293px] text-center">
              <h1 className="font-Space font-bold text-2xl md:text-5xl uppercase">
                Contact Us
              </h1>
              <div className="h-4 bg-blue-1 -mt-4"></div>
            </div>
            <p className="font-OpenSans font-normal text-sm md:text-xl">
              Get in Touch with Us – We Value Your Feedback and Are Here to
              Answer Your Questions
            </p>
            <form className="gap-5 space-y-5 w-full">
              <div className="flex flex-col md:flex-row gap-5 mt-5">
                <div className="w-full flex flex-col gap-1">
                  <label className="font-Space font-bold text-sm md:text-base">
                    First Name
                  </label>
                  <input
                    type="text"
                    className="bg-gray-2 border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-gray-3 focus:text-white focus:outline-none"
                    placeholder="Insert Your First Name"
                  />
                </div>
                <div className="w-full flex flex-col gap-1">
                  <label className="font-Space font-bold text-sm md:text-base">
                    Last Name
                  </label>
                  <input
                    type="text"
                    className="bg-gray-2 border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-gray-3 focus:text-white focus:outline-none"
                    placeholder="Insert Your Last Name"
                  />
                </div>
              </div>
              <div className="w-full flex flex-col gap-1">
                <label className="font-Space font-bold text-sm md:text-base">
                  Email
                </label>
                <input
                  type="email"
                  className="bg-gray-2 border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-gray-3 focus:text-white focus:outline-none"
                  placeholder="Insert Your Email Address"
                />
              </div>
              <div className="w-full flex flex-col gap-1">
                <label className="font-Space font-bold text-sm md:text-base">
                  Subject
                </label>
                <input
                  type="text"
                  className="bg-gray-2 border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-gray-3 focus:text-white focus:outline-none"
                />
              </div>
              <div className="w-full flex flex-col gap-1">
                <label className="font-Space font-bold text-sm md:text-base">
                  Message
                </label>
                <textarea
                  type="text"
                  className="bg-gray-2 border-gray-3 border-[1px] w-full p-2 font-Space font-normal text-gray-3 focus:text-white focus:outline-none"
                />
              </div>
              <div className="w-full flex justify-end">
                <button className="w-[120px] md:w-[257px] lg:w-[387px] py-3 bg-blue-1 font-Space font-bold text-sm md:text-base">
                  Submit
                </button>
              </div>
            </form>
          </div>
          <div className="w-full lg:w-[388px] text-white space-y-5">
            <h1 className="font-Space font-medium text-xl md:text-2xl uppercase">
              Feel Free to Get In Touch!
            </h1>
            <div className="flex items-center gap-3">
              <Phone />
              <p className="font-OpenSans font-normal text-sm md:text-xl">
                +62-812 1234 1234
              </p>
            </div>
            <div className="flex items-center gap-3">
              <Mail />
              <p className="font-OpenSans font-normal text-sm md:text-xl">
                chromylab@email.com
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
