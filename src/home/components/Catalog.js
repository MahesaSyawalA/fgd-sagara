import React from "react";
import { ImgCover } from "../../data";
import Search from "../../assets/icons/search";
import { useNavigate } from "react-router-dom";

export default function Catalog() {
  const navigate = useNavigate();
  return (
    <section id="catalog">
      <div className="flex flex-col container mx-auto px-5 gap-5">
        <div className="w-auto h-auto">
          <div className="w-40 md:w-[271px] h-[22px] text-center ">
            <h1 className="font-Space font-bold text-lg md:text-3xl uppercase text-black">
              E-book catalog
            </h1>
            <div className="w-full h-3 bg-blue-1 -mt-3.5"></div>
          </div>
        </div>
        <div className="flex items-center gap-3">
          <div className="relative w-full">
            <input
              placeholder="Search from book name, author, etc"
              className="rounded-none focus:shadow-none w-full h-10 p-2 border-gray-3 border-[1px] focus:outline-none pl-10"
            />
            <Search />
          </div>
          <button className="rounded-none border-none text-white bg-blue-2 hover:bg-blue-1 w-[182px] h-auto py-[10px]">
            Search
          </button>
        </div>
        <div className="grid grid-cols-2 lg:grid-cols-4 justify-items-center gap-4">
          {ImgCover.map((cover, index) => (
            <div
              className="p-3 border border-gray-5 gap-4 space-y-4 w-full h-full"
              key={index}
            >
              <img
                src={cover.image}
                className="w-full h-auto"
                alt="cover"
              />
              <div>
                <h1 className="font-Space font-medium text-xs md:text-sm lg:text-base">
                  {cover.title}
                </h1>
                <p className="font-OpenSans font-normal text-xs md:text-sm lg:text-base">
                  {cover.author}
                </p>
              </div>
              <button onClick={() => navigate('/e-book')} className="w-full bg-blue-1 py-2 text-white font-Space font-bold text-xs md:text-sm lg:text-base">
                See E-Book
              </button>
            </div>
          ))}
        </div>
      </div>
      <div className="flex items-center justify-center my-3">
        <hr className="w-[84px] md:w-[241px] lg:w-[539px] h-0" />
        <button className="text-gray-1 rounded-none border font-Space font-normal text-sm gap-3 py-1 px-3">
          Load More
        </button>
        <hr className="w-[84px] md:w-[241px] lg:w-[539px] h-0" />
      </div>
    </section>
  );
}
