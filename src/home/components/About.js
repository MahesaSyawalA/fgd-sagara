import React from "react";

export default function About() {
  return (
    <section id="about">
      <div className="bg-gray-6 w-full h-full flex flex-col lg:flex-row justify-around p-9 md:p-[70px] gap-20">
        <div className="bg-gray-1 w-[290px] md:w-[604px] lg:w-96 h-40 md:h-[340px] lg:h-auto rounded-lg mx-auto" />
        <div className="flex flex-col lg:w-1/2">
          <div className="h-7 w-[106px] text-center">
            <h1 className="font-Space font-bold text-2xl text-gray-1 uppercase">
              What is
            </h1>
            <div className="w-full h-3 bg-blue-1 -mt-3.5"></div>
          </div>
          <div className="h-[58px] w-[271px] md:w-[526px] text-center">
            <h1 className="font-Space font-bold text-2xl md:text-5xl text-gray-1 uppercase">
              Sagara Perspective?
            </h1>
            <div className="w-full h-4 bg-blue-1 -mt-3.5"></div>
          </div>
          <p className="font-OpenSans font-normal text-sm md:text-lg text-gray-1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et massa
            mi. Aliquam in hendrerit urna. Pellentesque sit amet sapien
            fringilla, mattis ligula consectetur, ultrices mauris. Maecenas
            vitae mattis tellus. Nullam quis imperdiet augue. Vestibulum auctor
            ornare leo, non suscipit magna interdum eu. Curabitur pellentesque
            nibh nibh, at maximus ante fermentum sit amet. Pellentesque commodo
            lacus at sodales sodales. Quisque sagittis orci ut diam condimentum,
            vel euismod erat placerat. In iaculis arcu eros, eget tempus orci
            facilisis id.
          </p>
        </div>
      </div>
    </section>
  );
}
