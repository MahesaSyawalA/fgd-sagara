import React from "react";

export default function Hero() {
  return (
    <section id="home">
      <div className=" w-full flex bg-cover bg-no-repeat bg-bottom justify-center h-[434px] md:h-[809px] lg:h-[700px] xl:h-[800px] 2xl:h-[900px] bg-[url('./assets/image/bg-sm.svg')] md:bg-[url('./assets/image/bg-md.svg')] lg:bg-[url('./assets/image/bg-lg.svg')] ">
        <div className=" mt-[41px] md:mt-[48px] static lg:absolute flex flex-col items-center lg:items-start p-0 gap-[16px] md:gap-[20px] lg:gap-[40px] w-[290px] md:w-[488px] lg:w-[488px] h-[115px] md:h-[254px] lg:h-[254px] sm:left-[26px] lg:left-[120px] lg:top-[183px] ">
          <div className="flex flex-col text-center lg:text-start items-center lg:items-end p-0 gap-[12px] lg:gap-5 w-full lg:w-[488px] h-[155px] md:h-[170px] lg:h-[170px]">
            <h1 className="font-Space w-[290px] md:w-[488px] lg:w-[488px] h-[69px] md:h-[126px] lg:h-[126px] font-bold text-[16px] md:text-[32px] lg:text-[32px] leading-[19.2px] md:leading-[40px] lg:leading-[40px] tracking-[-0.011em]  ">
              Unlocking Insights
              <p className=" leading-[24px] md:leading-[32px] lg:leading-[32px] ">
                in the Tech Industry with,
              </p>
              <p className=" leading-[24px] md:leading-[48px] lg:leading-[48px] text-[20px]  md:text-[40px] lg:text-[40px] text-[#2D9CDB] ">
                Sagara Perspective!
              </p>
            </h1>
            <p className=" w-full h-[34px] md:h-[24px] lg:h-[24px] font-OpenSans font-normal text-[14px] md:text-[20px] lg:text-[20px] leading-[120%] self-stretch text-[#333333] tracking-[-0.011em] ">
              Explore the Latest E-Books for Tech Enthusiasts
            </p>
          </div>
          <button className=" flex bg-[#2D9CDB] flex-col justify-center items-center w-[144px] md:w-[387px] lg:w-[387px] h-[34px] lg:h-[44px] ">
            <div className="flex flex-row items-center justify-center w-[162px] h-[34px] ">
              <p className="font-Space font-bold text-[14px] md:text-[16px] lg:text-[16px] leading-[125%] tracking-[-0.011em] text-white ">
                Browse E-Books
              </p>
            </div>
          </button>
        </div>
      </div>
    </section>
  );
}
